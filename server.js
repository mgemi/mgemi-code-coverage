const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const exec = require('child_process').exec;
const path = require('path');
const app = express();
const reportsDir = path.join(__dirname, 'reports');

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: true }));

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods','GET,POST');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

/**
 * Use this endpoint to record code coverage data.
 */
app.post('/send', function(req, res) {
  let body = req.body;
  let file = path.join(reportsDir, `${body.type}-${body.id}-${Date.now()}.json`);
  fs.writeFile(file, JSON.stringify(req.body.report), 'utf8');
  console.log('Recorded code coverage data in file', file);
  res.setHeader('Content-Type', 'application/json');
  res.status(200).send('SUCCESS');
});

/**
 * Starts the server
 */
module.exports.start = function() {
  var server = app.listen(3000, function () {
    console.log('app running on port', server.address().port);
    if (fs.existsSync(reportsDir)) {
      exec(`rm -rf ${reportsDir}`, function(err,out) {
        if (err) {
          console.log(err);
        }
        else {
          console.log('Removed previous code coverage data.', reportsDir);
          fs.mkdirSync(reportsDir);
        }
      });
    }
    else {
      fs.mkdirSync(reportsDir);
    }
  });
};
