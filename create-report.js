const fs = require('fs');
const istanbul = require('istanbul');
const path = require('path');
const collector = new istanbul.Collector();
const exec = require('child_process').exec;


if (!fs.existsSync(path.join(__dirname, 'reports'))) {
  fs.mkdirSync(path.join(__dirname, 'reports'));
}

module.exports = () => {
  fs.readdir(path.join(__dirname, 'reports'), (err, files) => {
    files.forEach(function (f) {
      if (!/\.json$/.test(f)) {
        return;
      }
      //each coverage object can have overlapping information about multiple files
      let filePath = path.join(__dirname, 'reports', f);
      let fileContent = fs.readFileSync(filePath);
      let json = JSON.parse(fileContent);
      collector.add(json);
    });

    // convenience method: do not use this when dealing with a large number of files
    let finalCoverage = collector.getFinalCoverage();

    fs.writeFile(path.join(__dirname, 'coverage.json'), JSON.stringify(finalCoverage), 'utf8');
    generate();
  })
};

function generate() {
  let configPath = path.join(__dirname, '.istanbul.yml');
  let rootPath = __dirname;
  let dirPath = path.join(process.cwd(), 'coverage-report');

  let command = `istanbul report --config ${configPath} --root ${rootPath} --dir ${dirPath}`;
  exec(command, (error) => {
    if (error) {
      console.log(error);
      return;
    }
  });
}
