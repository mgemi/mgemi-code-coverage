#!/usr/bin/env node
const argv = require('yargs').argv;
const server = require('./server');
const createReport = require('./create-report');
const command = argv._.pop();

// The server is used to record code coverage data. The report
// command generates a report from the data sent to the server.
if (command === 'start') {
  console.log('Starting server...');
  server.start();
}
else if (command === 'report') {
  console.log('Creating report...');
  createReport();
}
